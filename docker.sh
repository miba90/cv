#!/usr/bin/env bash

IMG_NAME=registry.gitlab.com/miba90/cv

for i in "$@" ; do
    if [[ $i == "-b" ]] ; then
        docker build --rm -t ${IMG_NAME} .
    fi
done

for i in "$@" ; do
    if [[ $i == "-r" ]] ; then
      docker rm -f $(docker ps -q $(grep -s $(docker images | grep none | awk '{ print $3 }')))
      docker ps -a | awk '{ print $1,$2 }' | grep ${IMG_NAME} | awk '{print $1}' | xargs -I {} docker rm -f {}
      docker run -dt -p 8080:8080 ${IMG_NAME}
    fi
    if [[ $i == "-p" ]] ; then
      docker push ${IMG_NAME}
    fi
done
