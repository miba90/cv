import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './components/_app/app.component';
import { LeftPanelComponent } from './components/left-panel/left-panel.component';
import { SkillsPanelComponent } from './components/skills-panel/skills-panel.component';
import { ExperiencesPanelComponent } from './components/experiences-panel/experiences-panel.component';
import { EducationPanelComponent } from './components/education-panel/education-panel.component';
import { InterestsPanelComponent } from './components/interests-panel/interests-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    LeftPanelComponent,
    SkillsPanelComponent,
    ExperiencesPanelComponent,
    EducationPanelComponent,
    InterestsPanelComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
