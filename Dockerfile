FROM node:6.7
MAINTAINER cv
EXPOSE 8080

COPY . /cv
WORKDIR /cv

RUN npm i -g http-server

#TODO: -prod
#RUN npm i
RUN npm run build.prod

CMD http-server ./dist

